-- vacuum;


-- check memory setting. task consume lots of memory.
-- select * from pg_settings where name like '%mem%';

 -- select distinct gemeentenaam, 'https://ds.vboenergie.commondatafactory.nl/list/?format=csv&match-gemeentecode=' || gemeentecode from baghelp.buurt_wijk_gemeente_provincie bwgp order by gemeentenaam;

select * from kvk.kvk_expand_sbi kes;


CREATE OR REPLACE FUNCTION random_between(low INT ,high INT)
   RETURNS INT AS
$$
BEGIN
   RETURN floor(random()* (high-low + 1) + low);
END;
$$ language 'plpgsql' STRICT;

create index on baghelp.num_buurt_wijk_gemeente_provincie (buurtcode);
analyze;

drop table if exists kvk.kvk_expand_sbi_test;
create table kvk.kvk_expand_sbi_test as (
	select * from kvk.kvk_expand_sbi limit 2000
)


select * from kvk.kvk_expand_sbi_test;

analyze;

-- check if indexes exist.
create index on kvk.kvk_expand_sbi using gist (geopunt);
create index on baghelp.all_pvsl_nums (geopunt);
create index on baghelp.num_buurt_wijk_gemeente_provincie (numid);

drop table if exists kvk.kvk_expand_sbi_anoniem;

create table kvk.kvk_expand_sbi_anoniem as (
SELECT
	random_between(100000, 200000)::text as dossiernr,
	random_between(200000, 300000)::text as subdossier,
	random_between(300000, 400000)::text as vgnummer,
	substr(md5(random()::text), 0, 2)::text || ' ' || activiteit as hn_1x45,
	hoofdact,
	nevact1,
	nevact2,
	0 as huisnr,
	'' as toev_hsnr,
	substr(md5(random()::text), 0, 45) as nm_1x45,
	to_char(
	   timestamp '1990-01-10 20:00:00' +
       random() * (timestamp '1990-01-20 20:00:00' -
                   timestamp '2030-01-10 10:00:00'), 'YYYYMMDD') as dat_vest,
       to_char(
	   timestamp '1990-01-10 20:00:00' +
       random() * (timestamp '1990-01-20 20:00:00' -
                   timestamp '2030-01-10 10:00:00'), 'YYYYMMDD') as dat_inschr,
	to_char(
	   timestamp '1990-01-10 20:00:00' +
       random() * (timestamp '1990-01-20 20:00:00' -
                   timestamp '2030-01-10 10:00:00'), 'YYYYMMDD') as dat_oprich,
	-- dat_oprich,
	'ri' as red_inschr,
	'ru' as red_uitsch,
	'ro' as red_opheff,
	substr(md5(random()::text), 0, 15) as vennaam,
	random_between(0, 200000)::text as gestortkap,
	'' as stat_insch, -- statutaire inschrijving?
	'' as mobiel_nr,
	rv_fijn,          -- rechtsvorm
	'https://demo.bedrijf.com' as url,
	---
	bh.pid,
	bh.numid,
	bh.vid,
	bh.sid,
	bh.lid,
	bh.huisnummer,
	bh.huisletter,
	bh.huisnummertoevoeging,
	bh.postcode,
	bh.oppervlakte,
	---
	match_score,
	---
	bh.geopunt,
	'DEMO_' || buurtnaam as buurtnaam,
    'DEMO_' || buurtcode as buurtcode,
    'DEMO_' || gemeentenaam as gemeentenaam,
    'DEMO_' || gemeentecode as gemeentecode,
    provincienaam, provinciecode,
	---
	omschrijving_rechtsvormcode,
	-- geometry,
	ST_Transform(ST_SetSRID(bh.geopunt,28992 ), 3857) as geometry, -- set new geometry
	-- dist,
	sbicode, activiteit, 
	l1_code, l1_title, l2_code, l2_title, l3_code, l3_title, 
	l4_code, l4_title, l5_code, l5_title
	-- nextval('kvk.kvk_expand_sbi_id_seq'::regclass) as id -- generate new ids
	--id
FROM kvk.kvk_expand_sbi e
cross join lateral (
    select
		apn.pid,
		apn.numid,
		apn.vid,
		apn.sid,
		apn.lid,
		apn.huisnummer,
		apn.huisletter,
		apn.huisnummertoevoeging,
		apn.postcode,
		apn.geopunt,
		geopunt <-> apn.geopunt as dist,
		apn.oppervlakte
		from baghelp.num_buurt_wijk_gemeente_provincie nbwgp
		left join baghelp.all_pvsl_nums apn  on nbwgp.numid = apn.numid
		where nbwgp.buurtcode = e.buurtcode and e.geopunt <-> apn.geopunt > 15 and e.geopunt <-> apn.geopunt < 50
		and apn.numid != e.numid order by random()
		limit 1
		 -- make sure it is a different address
	) as bh
where gemeentecode = 'GM0848'
);

-- copy new demo data into existing municipality
insert into kvk.kvk_expand_sbi select * from kvk.kvk_expand_sbi_anoniem;

select * from kvk.kvk_expand_sbi kes;


CREATE index on kvk.kvk_expand_sbi_anoniem using gist (geometry);
ALTER table kvk.kvk_expand_sbi_anoniem  ALTER column geometry type geometry(point, 3857);

-- ALTER TABLE kvk.kvk_expand_sbi_anoniem  ADD COLUMN id SERIAL PRIMARY KEY;

select * from kvk.kvk_expand_sbi_anoniem;

create




select * from (
    select
		--pid,
		apn.numid,
		st_setsrid(st_POINT(150836.789, 411082.194), 28992) source_point,
		apn.geopunt,
		st_setsrid(st_POINT(150836.789, 411082.194), 28992) <-> apn.geopunt as dist,
		apn.vid,
		apn.sid,
		apn.lid,
		apn.huisnummer,
		apn.huisletter,
		apn.huisnummertoevoeging,
		apn.postcode,
		-- apn.geopunt,
		oppervlakte
		from baghelp.num_buurt_wijk_gemeente_provincie nbwgp
		left join baghelp.all_pvsl_nums apn  on nbwgp.numid = apn.numid
		where nbwgp.buurtcode = 'BU07960211'
		AND st_setsrid(st_POINT(150836.789, 411082.194), 28992) <-> apn.geopunt > 50
		and st_setsrid(st_POINT(150836.789, 411082.194), 28992) <-> apn.geopunt < 100
		-- and apn.numid != e.numid  -- make sure it is a different address
		-- order by dist
		order by random()
		limit 1
	) nearby

