/*

        KVK dataset ETL to create cleaned datasets ready for vector maps / data-selection

        RSI values.
        0 Rubriek is ongewijzigd
        1 Rubriek is gewijzigd
        2 Rubriek is opgeheven / geleegd
        3 Rubriek is ingevoerd / gevuld

*/

/*
        Load xls into csv import schema
        using xls2csv/ssconvert en pgfutter (manual for now)
*/

-- SELECT * FROM import.kvk_initieel ki;
-- select * from import.kvk_mutaties km left outer join import.kvk_initieel ki on ki.dossiernr = km.dossiernr;

drop table kvk.kvk_actueel_092023;

-- check for duplicates --
select dossiernr, subdossier, vgnummer, hn_1x45, count(*) as dupes from kvk.kvk_actueel_092023
group by dossiernr, subdossier, vgnummer, hn_1x45
having count(*) > 1

-- fill initial table into kvk -
-- select * into kvk.kvk_actueel_082023 from (
-- select * from kvkimport.initieel_082023__xx
-- union
-- select * from kvkimport.initieel_20230705__xx
-- ) allkvk

-- copy previous into new kvk set.

select *
into kvk.kvk_actueel_092023
from kvk.kvk_actueel_082023


select count(*) from kvk.kvk_actueel_092023 kav;
select count(*) from kvk.kvk_actueel_082023 kav;

/* deal with mutations files */

/* add new rows from mutation - idempotent */
insert into kvk.kvk_actueel_092023
select * from kvkimport.mutaties_092023 km where not exists (
    select 1 from kvk.kvk_actueel_092023 ka where ka.dossiernr=km.dossiernr and ka.subdossier=km.subdossier and ka.vgnummer=km.vgnummer)


/* update rows from mutation - idempotent */
UPDATE kvk.kvk_actueel_092023 ka
SET
    dossiernr=km.dossiernr,
    subdossier=km.subdossier,
    reg_ltr=km.reg_ltr,
    hfdzk_fil=km.hfdzk_fil,
    rsi=km.rsi,
    vgnummer=km.vgnummer,
    rsi_1=km.rsi_1,
    dsnr_hzkey=km.dsnr_hzkey,
    subd_hzkey=km.subd_hzkey,
    rsi_2=km.rsi_2,
    hn_1x45=km.hn_1x45,
    rsi_3=km.rsi_3,
    hn_1x2x30=km.hn_1x2x30,
    hn_2x2x30=km.hn_2x2x30,
    rsi_4=km.rsi_4,
    hn_1x30=km.hn_1x30,
    rsi_5=km.rsi_5,
    straatnaam=km.straatnaam,
    rsi_7=km.rsi_7,
    huisnr=km.huisnr,
    rsi_8=km.rsi_8,
    toev_hsnr=km.toev_hsnr,
    rsi_9=km.rsi_9,
    postcode=km.postcode,
    rsi_11=km.rsi_11,
    woonplaats=km.woonplaats,
    rsi_12=km.rsi_12,
    strnaam_ca=km.strnaam_ca,
    rsi_14=km.rsi_14,
    hsnr_ca=km.hsnr_ca,
    rsi_15=km.rsi_15,
    toevhn_ca=km.toevhn_ca,
    rsi_16=km.rsi_16,
    postcd_ca=km.postcd_ca,
    rsi_18=km.rsi_18,
    filler=km.filler,
    vsi=km.vsi,
    woonpl_ca=km.woonpl_ca,
    rsi_19=km.rsi_19,
    rv_fijn=km.rv_fijn,
    rsi_20=km.rsi_20,
    hoofdact=km.hoofdact,
    nevact1=km.nevact1,
    nevact2=km.nevact2,
    rsi_21=km.rsi_21,
    kwp_totaal=km.kwp_totaal,
    rsi_22=km.rsi_22,
    kwp_ft=km.kwp_ft,
    rsi_23=km.rsi_23,
    import=km.import,
    export=km.export,
    rsi_24=km.rsi_24,
    telabnr=km.telabnr,
    telnetnr=km.telnetnr,
    rsi_24b=km.rsi_24b,
    ind_faill=km.ind_faill,
    rsi_26=km.rsi_26,
    ind_surs=km.ind_surs,
    rsi_27=km.rsi_27,
    beh_knr=km.beh_knr,
    rsi_29=km.rsi_29,
    fc=km.fc,
    rsi_30=km.rsi_30,
    nm_1x30=km.nm_1x30,
    rsi_31=km.rsi_31,
    nm_1x2x30=km.nm_1x2x30,
    nm_2x2x30=km.nm_2x2x30,
    rsi_32=km.rsi_32,
    nm_1x45=km.nm_1x45,
    rsi_33=km.rsi_33,
    briefaanh=km.briefaanh,
    rsi_34=km.rsi_34,
    aanspreekt=km.aanspreekt,
    rsi_35=km.rsi_35,
    titel1=km.titel1,
    titel2=km.titel2,
    vrletters=km.vrletters,
    vrvoegsels=km.vrvoegsels,
    achternm=km.achternm,
    rsi_36=km.rsi_36,
    sexe_code=km.sexe_code,
    rsi_37=km.rsi_37,
    dat_vest=km.dat_vest,
    rsi_38=km.rsi_38,
    huidig_adr=km.huidig_adr,
    rsi_39=km.rsi_39,
    dat_inschr=km.dat_inschr,
    rsi_40=km.rsi_40,
    dat_oprich=km.dat_oprich,
    rsi_41=km.rsi_41,
    dat_opheff=km.dat_opheff,
    rsi_42=km.rsi_42,
    dat_voorz=km.dat_voorz,
    rsi_43=km.rsi_43,
    red_inschr=km.red_inschr,
    rsi_44=km.rsi_44,
    red_uitsch=km.red_uitsch,
    rsi_45=km.rsi_45,
    red_opheff=km.red_opheff,
    rsi_46=km.rsi_46,
    vennaam=km.vennaam,
    rsi_47=km.rsi_47,
    gestortkap=km.gestortkap,
    rsi_48=km.rsi_48,
    geplkap=km.geplkap,
    rsi_49=km.rsi_49,
    maatschkap=km.maatschkap,
    rsi_50=km.rsi_50,
    stat_insch=km.stat_insch,
    rsi_51=km.rsi_51,
    exwptotaal=km.exwptotaal,
    rsi_52=km.rsi_52,
    peildatwp=km.peildatwp,
    rsi_53=km.rsi_53,
    mobiel_nr=km.mobiel_nr,
    rsi_54=km.rsi_54,
    url=km.url,
    rsi_55=km.rsi_55,
    nmi=km.nmi,
    rsi_56=km.rsi_56,
    rsin=km.rsin,
    rsi_57=km.rsi_57
from kvkimport.mutaties_092023 km
where km.dossiernr = ka.dossiernr and km.subdossier=ka.subdossier and km.vgnummer=ka.vgnummer;


/*
   Create new BAG nummeraanduiding matched dataset
   Where we match with 1 addresss used for BAG ids / keys and geo locations

 */

drop table if exists kvk.kvk_actueel_bag_092023;
select
    dossiernr,
    subdossier,
    vgnummer,
    hn_1x45,
    hoofdact,
    nevact1,
    nevact2,
    huisnr,
    toev_hsnr,
    nm_1x45,
    dat_vest,
    dat_inschr,
    dat_oprich,
    red_inschr,
    red_uitsch,
    red_opheff,
    vennaam,
    gestortkap,
    stat_insch,
    mobiel_nr,
    rv_fijn,
    url,
    a.*
into kvk.kvk_actueel_bag_092023
from kvk.kvk_actueel_092023 ki,
lateral ( select
                --coalesce (pid, sid, lid) as matched_id,
                pa.pid,
                pa.numid,
                pa.vid,
                pa.sid,
                pa.lid,
                pa.huisnummer, pa.huisletter, pa.huisnummertoevoeging,
                pa.postcode,
                pa.oppervlakte,
                levenshtein(concat(pa.huisnummer, ' ', lower(pa.huisletter) , lower(pa.huisnummertoevoeging)),
                            concat(ki.huisnr,     ' ', lower(ki.toev_hsnr) )) as match_score,
                pa.geopunt
          from baghelp.all_pvsl_nums pa
          where pa.postcode = ki.postcode
          -- and pa.huisnummer = r.huisnummerint
          order by levenshtein(concat(pa.huisnummer, ' ', lower(pa.huisletter) , lower(pa.huisnummertoevoeging)),
                               concat(ki.huisnr,     ' ', lower(ki.toev_hsnr) ))  asc
          limit 1
) a

/*
 * Add administration codes province - gemeente - wijk - buurt to each record.
 */
select * from kvk.kvk_actueel_092023;
select * from kvk.kvk_actueel_bag_092023 kav;


select
    kab.*,
    nbwgp.buurtnaam,
    buurtcode,
    gemeentenaam,
    gemeentecode,
    nbwgp.naam as provincienaam,
    nbwgp.identificatie as provinciecode
into kvk.kvk_actueel_bag_codes_092023
from kvk.kvk_actueel_bag_092023 kab
left outer join baghelp2023.num_buurt_wijk_gemeente_provincie nbwgp on (nbwgp.numid = kab.numid )

/**

    Create new tables used for publishing.

**/

/**
    Merge kvk vestigingen met CBS SBI codes activiteiten
    uses sbi code from https://gitlab.com/commondatafactory/sbi-codes
    which extracts sbi codes from
    http://sbi.cbs.nl/cbs.typeermodule.typeerservicewebapi
**/

drop table if exists kvk.kvk_expand_sbi cascade;

select
        ka.*,
        r.omschrijving_rechtsvormcode,
        ST_Transform(ST_SetSRID(geopunt,28992 ), 3857) as geometry,
        code as sbicode,
        title as activiteit,
        sbi_tree -> 'l1' ->> 0 as l1_code,
        sbi_tree -> 'l1' ->> 1 as l1_title,
        sbi_tree -> 'l2' ->> 0 as l2_code,
        sbi_tree -> 'l2' ->> 1 as l2_title,
        sbi_tree -> 'l3' ->> 0 as l3_code,
        sbi_tree -> 'l3' ->> 1 as l3_title,
        sbi_tree -> 'l4' ->> 0 as l4_code,
        sbi_tree -> 'l4' ->> 1 as l4_title,
        sbi_tree -> 'l5' ->> 0 as l4_code,
        sbi_tree -> 'l5' ->> 1 as l4_title
into kvk.kvk_expand_sbi
from kvk.kvk_actueel_bag_codes_092023 ka
left outer join kvkimport.rechtsvormcodes r on (r.rechtsvormcode = rv_fijn)
left outer join import.sbicodes s on (
        ka.hoofdact = s.code or
        ka.nevact1 = s.code or
        ka.nevact2 = s.code);

/*
        2023 - 09
        Adjust table to be ready for pg_tileserver

        TODO: script below code with a list of Gemeente Codes.
*/

ALTER TABLE kvk.kvk_expand_sbi ADD COLUMN id SERIAL PRIMARY KEY;
CREATE index on kvk.kvk_expand_sbi using gist (geometry);
ALTER table kvk.kvk_expand_sbi alter column geometry type geometry(point, 3857);

/* Create VIEWS for users */

drop view if exists kvk.kvk_expand_sbi_GM0848;
create view kvk.kvk_expand_sbi_GM0848 as ( select * from kvk.kvk_expand_sbi kes where kes.gemeentecode = 'GM0848');

drop view if exists kvk.kvk_expand_sbi_GM0796;
create view kvk.kvk_expand_sbi_GM0796 as ( select * from kvk.kvk_expand_sbi kes where kes.gemeentecode = 'GM0796');

drop view if exists kvk.kvk_expand_sbi_GM1926;
create view kvk.kvk_expand_sbi_GM1926 as ( select * from kvk.kvk_expand_sbi kes where kes.gemeentecode = 'GM1926');

drop view if exists kvk.kvk_expand_sbi_GM0861;
create view kvk.kvk_expand_sbi_GM0861 as ( select * from kvk.kvk_expand_sbi kes where kes.gemeentecode = 'GM0861');


select * from kvk.kvk_expand_sbi_GM0796;
select * from kvk.kvk_expand_sbi_gm0848;
select * from kvk.kvk_expand_sbi_GM1926;
select * from kvk.kvk_expand_sbi_gm0861;

select count(*) from kvk.kvk_expand_sbi;

SELECT set_config('log_statement', 'all', false);

