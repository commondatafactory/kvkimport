create index on kvk.kvk_expand_sbi (gemeentecode);

--drop function kvk.muni

drop function kvk.municipality_select(int4, int4, int4, text);

CREATE OR REPLACE
FUNCTION kvk.municipality_select(
            z integer, x integer, y integer,
            gmcode text default 'missing')
RETURNS bytea
AS $$
DECLARE
    result bytea;
BEGIN
    WITH
    bounds AS (
      SELECT ST_TileEnvelope(z, x, y) AS geom
    ),
    mvtgeom AS (
      SELECT
      	ST_AsMVTGeom(ST_Transform(k.geometry , 3857), bounds.geom) AS geometry,
        dossiernr,
        vgnummer,
        subdossier,
        hn_1x45,
        hoofdact,
        nevact1, nevact2,
        huisnr, toev_hsnr,
        nm_1x45,
        dat_vest, dat_inschr, dat_oprich, red_inschr, red_uitsch, red_opheff,
        vennaam, gestortkap, stat_insch,
        -- mobiel_nr,
        rv_fijn, -- rechtsvorm
        url, pid, numid, vid, sid, lid, huisnummer, huisletter, huisnummertoevoeging,
        postcode, oppervlakte, match_score,
        --geoopunt,
        buurtnaam, buurtcode, gemeentenaam, gemeentecode, provincienaam, provinciecode, omschrijving_rechtsvormcode
        sbicode,
        activiteit,
        l1_code, l1_title,
        l2_code, l2_title,
        l3_code, l3_title,
        l4_code, l4_title,
        l5_code, l5_title,
        id
      FROM kvk.kvk_expand_sbi k, bounds
      WHERE ST_Intersects(k.geometry, ST_Transform(bounds.geom, 3857))
      AND k.gemeentecode = ANY (string_to_array(gmcode, ','))
    )
    SELECT ST_AsMVT(mvtgeom, 'default')
    INTO result
    FROM mvtgeom;

    RETURN result;
END;
$$
LANGUAGE 'plpgsql'
STABLE
PARALLEL SAFE;

COMMENT ON FUNCTION kvk.municipality_select IS 'Filters the kvk data table by municipality code using the "gmcode" (a, b, c) parameter.';


/*
dossiernr
subdossier
vgnummer
hn_1x45
hoofdact
nevact1
nevact2
huisnr
toev_hsnr
nm_1x45
dat_vest
dat_inschr
dat_oprich
red_inschr
red_uitsch
red_opheff
vennaam
gestortkap
stat_insch
mobiel_nr
rv_fijn
url
pid
numid
vid
sid
lid
huisnummer
huisletter
huisnummertoevoeging
postcode
oppervlakte
match_score
geopunt
buurtnaam
buurtcode
gemeentenaam
gemeentecode
provincienaam
provinciecode
omschrijving_rechtsvormcode
geometry
sbicode
activiteit
l1_code
l1_title
l2_code
l2_title
l3_code
l3_title
l4_code
l4_title
l5_code
l5_title
id
*/
