/*

	KVK dataset ETL to create cleaned datasets ready for vector maps / data-selection

	RSI values.
	0 Rubriek is ongewijzigd
	1 Rubriek is gewijzigd
	2 Rubriek is opgeheven / geleegd
	3 Rubriek is ingevoerd / gevuld

*/

/*
	Load xls into csv import schema
	using xls2csv/ssconvert en pgfutter (manual for now)
*/

-- SELECT * FROM import.kvk_initieel ki;
-- select * from import.kvk_mutaties km left outer join import.kvk_initieel ki on ki.dossiernr = km.dossiernr;

drop table kvk.kvk_actueelv3;

-- fill initial table into kvk.
select *
into kvk.kvk_actueel_v3
from kvkimport.denbosh_initieel_deel_1;

insert into kvk.kvk_actueel_v3 select * from kvkimport.denbosh_initieel_deel_2;

select count(*) from kvk.kvk_actueel_v3 kav;

--select count(*) from import.kvk_mutaties km;

--insert into kvk.kvk_actueel select * from kvkimport.denbosh_initieel_deel_1_xls;

--select * from kvkimport.pekela_kvk_mutaties_xls pkmx;
--select * from kvk.kvk_actueel ka;

--select * from kvkimport.denbosh_initieel_deel_1_xls;
--select * from kvkimport.denbosh_initieel_deel_2_xls;

--insert into kvk.kvk_actueel select * from kvkimport.denbosh_initieel_deel_2_xls;
-- apply mutation files

--select * from kvk.kvk_actueel_v2 kav


/* add new rows from mutation - idempotent */
insert into kvk.kvk_actueel
select * from import.kvk_mutaties km where not exists (
    select 1 from kvk.kvk_actueel ka where ka.dossiernr=km.dossiernr and ka.subdossier=km.subdossier)

/* update rows from mutation */
update kvk.kvk_actueel ka
set
  filler      = km.filler,
  filler2     = km.filler2,
  hn_1x2x30   = km.hn_1x2x30,
  hn_1x30     = km.hn_1x30,
  hn_1x45     = km.hn_1x45,
  hn_2x2x30   = km.hn_2x2x30,
  hoofdact    = km.hoofdact,
  hsnr_ca     = km.hsnr_ca,
  huisnr      = km.huisnr,
  nevact1     = km.nevact1,
  nevact2     = km.nevact2,
  nmi         = km.nmi,
  pc_wpl      = km.pc_wpl,
  pc_wpl_ca   = km.pc_wpl_ca,
  postcd_ca   = km.postcd_ca,
  postcode    = km.postcode,
  rgl         = km.rgl,
  rsi_10      = km.rsi_10,
  rsi_11      = km.rsi_11,
  rsi_12      = km.rsi_12,
  rsi_13      = km.rsi_13,
  rsi_14      = km.rsi_14,
  rsi_15      = km.rsi_15,
  rsi_16      = km.rsi_16,
  rsi_17      = km.rsi_17,
  rsi_18      = km.rsi_18,
  rsi_19      = km.rsi_19,
  rsi_2       = km.rsi_2,
  rsi_20      = km.rsi_20,
  rsi_23      = km.rsi_23,
  rsi_3       = km.rsi_3,
  rsi_4       = km.rsi_4,
  rsi_5       = km.rsi_5,
  rsi_6       = km.rsi_6,
  rsi_7       = km.rsi_7,
  rsi_8       = km.rsi_8,
  rsi_9       = km.rsi_9,
  str_hs_ca   = km.str_hs_ca,
  str_hsnr    = km.str_hsnr,
  straatnaam  = km.straatnaam,
  strnaam_ca  = km.strnaam_ca,
  toev_hsnr   = km.toev_hsnr,
  toevhn_ca   = km.toevhn_ca,
  vg_nr       = km.vg_nr,
  vsi         = km.vsi,
  woonpl_ca   = km.woonpl_ca,
  woonplaats  = km.woonplaats
from import.kvk_mutaties km
where km.dossiernr = ka.dossiernr and km.subdossier=ka.subdossier;


/*
   Create new BAG nummeraanduiding matched dataset
   Where we match with 1 addresss used for BAG ids / keys and geo locations

 */

drop table if exists kvk.kvk_actueel_bag_v3;
select
    dossiernr,
	subdossier,
	vgnummer,
	hn_1x45,
    hoofdact,
	nevact1,
	nevact2,
    huisnr,
	toev_hsnr,
    nm_1x45,
    dat_vest,
	dat_inschr,
	dat_oprich,
    red_inschr,
	red_uitsch,
	red_opheff,
    vennaam,
	gestortkap,
    stat_insch,
    mobiel_nr,
    rv_fijn,
    url,
    a.*
into kvk.kvk_actueel_bag_v3
from kvk.kvk_actueel_v3 ki,
lateral ( select
                --coalesce (pid, sid, lid) as matched_id,
                pa.pid,
                pa.numid,
                pa.vid,
                pa.sid,
                pa.lid,
                pa.huisnummer, pa.huisletter, pa.huisnummertoevoeging,
                pa.postcode,
                pa.oppervlakte,
                levenshtein(concat(pa.huisnummer, ' ', lower(pa.huisletter) , lower(pa.huisnummertoevoeging)),
                            concat(ki.huisnr,     ' ', lower(ki.toev_hsnr) )) as match_score,
                pa.geopunt
          from baghelp.all_pvsl_nums pa
          where pa.postcode = ki.postcode
          -- and pa.huisnummer = r.huisnummerint
          order by levenshtein(concat(pa.huisnummer, ' ', lower(pa.huisletter) , lower(pa.huisnummertoevoeging)),
                               concat(ki.huisnr,     ' ', lower(ki.toev_hsnr) ))  asc
          limit 1
) a

/*
 * Add administration codes province - gemeente - wijk - buurt to each record.
 */
select * from kvk.kvk_actueel_v3;
select * from kvk.kvk_actueel_bag_v3 kav;


select kab.*, nbwgp.buurtnaam, buurtcode, gemeentenaam, gemeentecode, nbwgp.naam as provincienaam, nbwgp.identificatie as provinciecode
into kvk.kvk_actueel_bag_codes_v3
from kvk.kvk_actueel_bag_v3 kab
left outer join baghelp.num_buurt_wijk_gemeente_provincie nbwgp on (nbwgp.numid = kab.numid )


/*
	Merge kvk vestigingen met CBS SBI codes activiteiten
	uses sbi code from https://gitlab.com/commondatafactory/sbi-codes
	which extracts sbi codes from
	http://sbi.cbs.nl/cbs.typeermodule.typeerservicewebapi
*/
drop table if exists kvk.kvk_expand_sbi cascade;


select
	ka.*,
	ST_Transform(ST_SetSRID(geopunt,28992 ), 3857) as geometry,
	code as sbicode,
	title as activiteit,
	sbi_tree -> 'l1' ->> 0 as l1_code,
	sbi_tree -> 'l1' ->> 1 as l1_title,
	sbi_tree -> 'l2' ->> 0 as l2_code,
	sbi_tree -> 'l2' ->> 1 as l2_title,
	sbi_tree -> 'l3' ->> 0 as l3_code,
	sbi_tree -> 'l3' ->> 1 as l3_title,
	sbi_tree -> 'l4' ->> 0 as l4_code,
	sbi_tree -> 'l4' ->> 1 as l4_title
into kvk.kvk_expand_sbi
from kvk.kvk_actueel_bag_codes_v3 ka
left outer join import.sbicodes s on (
	ka.hoofdact = s.code or
	ka.nevact1 = s.code or
	ka.nevact2 = s.code);

/*
	2023 - 01

	Adjust table to be ready for pgtileserver

*/

ALTER TABLE kvk.kvk_expand_sbi ADD COLUMN id SERIAL PRIMARY KEY;
CREATE index on kvk.kvk_expand_sbi using gist (geometry);
ALTER table kvk.kvk_expand_sbi alter column geometry type geometry(point, 3857);

drop view if exists kvk.kvk_expand_sbi_GM0848;

create view kvk.kvk_expand_sbi_GM0848 as ( select * from kvk.kvk_expand_sbi kes where kes.gemeentecode = 'GM0848');

drop view if exists kvk.kvk_expand_sbi_GM0796;

create view kvk.kvk_expand_sbi_GM0796 as ( select * from kvk.kvk_expand_sbi kes where kes.gemeentecode = 'GM0796');


select * from kvk.kvk_expand_sbi_GM0796;
select * from kvk.kvk_expand_sbi_gm0848;
