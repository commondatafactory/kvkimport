/*

        KVK dataset ETL to create cleaned datasets ready for vector maps / data-selection

        RSI values.

        0 Rubriek is ongewijzigd
        1 Rubriek is gewijzigd
        2 Rubriek is opgeheven / geleegd
        3 Rubriek is ingevoerd / gevuld

*/

/*
        Load xls into csv import schema
        using xls2csv/ssconvert en pgfutter (manual for now)
*/

-- SELECT * FROM import.kvk_initieel ki;
-- select * from import.kvk_mutaties km left outer join import.kvk_initieel ki on ki.dossiernr = km.dossiernr;

-- IMPORTENT SET THESE.
@set prevk = kvk_actueel_052024
@set newk = kvk_actueel_062024


drop table if exists kvk.${newk};
-- copy previous month into new kvk set.
select * into kvk."${newk}" from kvk.${prevk};


/* check if present counts should be identical */
select count(*) from kvk."${newk}";
select count(*) from kvk."${prevk}";

-- OPTIONAL ADD NEW  initial table into kvk -
--insert into kvk."${newk}" select * from kvkimport.uitbreiding;

select count(*) from kvk.kvk_actueel_062024 kav;
select count(*) from kvk.kvk_actueel_052024 kav;
select count(*) from kvk.kvk_actueel_042024 kav;
select count(*) from kvk.kvk_actueel_032024 kav;
select count(*) from kvk.kvk_actueel_022024 kav;
select count(*) from kvk.kvk_actueel_012024 kav;
select count(*) from kvk.kvk_actueel_122023 kav;
select count(*) from kvk.kvk_actueel_092023 kav;

/* deal with mutations files 10 and 11 */

-- select count(*) from kvkimport.mutaties_052024 m;
/* add new rows from mutation - idempotent */
insert into kvk."${newk}"
select * from kvkimport.mutaties_06_2024 km where not exists (
    select 1 from kvk."${newk}" ka where ka.dossiernr=km.dossiernr and ka.subdossier=km.subdossier and ka.vgnummer=km.vgnummer)


/* update rows from mutation - idempotent */
UPDATE kvk."${newk}" ka
SET
    dossiernr=km.dossiernr,
    subdossier=km.subdossier,
    reg_ltr=km.reg_ltr,
    hfdzk_fil=km.hfdzk_fil,
    rsi=km.rsi,
    vgnummer=km.vgnummer,
    rsi_1=km.rsi_1,
    dsnr_hzkey=km.dsnr_hzkey,
    subd_hzkey=km.subd_hzkey,
    rsi_2=km.rsi_2,
    hn_1x45=km.hn_1x45,
    rsi_3=km.rsi_3,
    hn_1x2x30=km.hn_1x2x30,
    hn_2x2x30=km.hn_2x2x30,
    rsi_4=km.rsi_4,
    hn_1x30=km.hn_1x30,
    rsi_5=km.rsi_5,
    straatnaam=km.straatnaam,
    rsi_7=km.rsi_7,
    huisnr=km.huisnr,
    rsi_8=km.rsi_8,
    toev_hsnr=km.toev_hsnr,
    rsi_9=km.rsi_9,
    postcode=km.postcode,
    rsi_11=km.rsi_11,
    woonplaats=km.woonplaats,
    rsi_12=km.rsi_12,
    strnaam_ca=km.strnaam_ca,
    rsi_14=km.rsi_14,
    hsnr_ca=km.hsnr_ca,
    rsi_15=km.rsi_15,
    toevhn_ca=km.toevhn_ca,
    rsi_16=km.rsi_16,
    postcd_ca=km.postcd_ca,
    rsi_18=km.rsi_18,
    filler=km.filler,
    vsi=km.vsi,
    woonpl_ca=km.woonpl_ca,
    rsi_19=km.rsi_19,
    rv_fijn=km.rv_fijn,
    rsi_20=km.rsi_20,
    hoofdact=km.hoofdact,
    nevact1=km.nevact1,
    nevact2=km.nevact2,
    rsi_21=km.rsi_21,
    kwp_totaal=km.kwp_totaal,
    rsi_22=km.rsi_22,
    kwp_ft=km.kwp_ft,
    rsi_23=km.rsi_23,
    import=km.import,
    export=km.export,
    rsi_24=km.rsi_24,
    telabnr=km.telabnr,
    telnetnr=km.telnetnr,
    rsi_24b=km.rsi_24b,
    ind_faill=km.ind_faill,
    rsi_26=km.rsi_26,
    ind_surs=km.ind_surs,
    rsi_27=km.rsi_27,
    beh_knr=km.beh_knr,
    rsi_29=km.rsi_29,
    fc=km.fc,
    rsi_30=km.rsi_30,
    nm_1x30=km.nm_1x30,
    rsi_31=km.rsi_31,
    nm_1x2x30=km.nm_1x2x30,
    nm_2x2x30=km.nm_2x2x30,
    rsi_32=km.rsi_32,
    nm_1x45=km.nm_1x45,
    rsi_33=km.rsi_33,
    briefaanh=km.briefaanh,
    rsi_34=km.rsi_34,
    aanspreekt=km.aanspreekt,
    rsi_35=km.rsi_35,
    titel1=km.titel1,
    titel2=km.titel2,
    vrletters=km.vrletters,
    vrvoegsels=km.vrvoegsels,
    achternm=km.achternm,
    rsi_36=km.rsi_36,
    sexe_code=km.sexe_code,
    rsi_37=km.rsi_37,
    dat_vest=km.dat_vest,
    rsi_38=km.rsi_38,
    huidig_adr=km.huidig_adr,
    rsi_39=km.rsi_39,
    dat_inschr=km.dat_inschr,
    rsi_40=km.rsi_40,
    dat_oprich=km.dat_oprich,
    rsi_41=km.rsi_41,
    dat_opheff=km.dat_opheff,
    rsi_42=km.rsi_42,
    dat_voorz=km.dat_voorz,
    rsi_43=km.rsi_43,
    red_inschr=km.red_inschr,
    rsi_44=km.rsi_44,
    red_uitsch=km.red_uitsch,
    rsi_45=km.rsi_45,
    red_opheff=km.red_opheff,
    rsi_46=km.rsi_46,
    vennaam=km.vennaam,
    rsi_47=km.rsi_47,
    gestortkap=km.gestortkap,
    rsi_48=km.rsi_48,
    geplkap=km.geplkap,
    rsi_49=km.rsi_49,
    maatschkap=km.maatschkap,
    rsi_50=km.rsi_50,
    stat_insch=km.stat_insch,
    rsi_51=km.rsi_51,
    exwptotaal=km.exwptotaal,
    rsi_52=km.rsi_52,
    peildatwp=km.peildatwp,
    rsi_53=km.rsi_53,
    mobiel_nr=km.mobiel_nr,
    rsi_54=km.rsi_54,
    url=km.url,
    rsi_55=km.rsi_55,
    nmi=km.nmi,
    rsi_56=km.rsi_56,
    rsin=km.rsin,
    rsi_57=km.rsi_57
from kvkimport.mutaties_06_2024 km
where km.dossiernr = ka.dossiernr and km.subdossier=ka.subdossier and km.vgnummer=ka.vgnummer;

-- detect duplications. first make a backup.
create table kvk.${newk}_backup as (
select * from kvk."${newk}" ka
)

-- restore if needed.
drop table if exists kvk.kvk_actueel_022024;
select * into kvk.kvk_actueel_022024 from kvk.kvk_actueel_022024_backup

-- list duplications.
select dossiernr, subdossier, vgnummer, hn_1x45, woonplaats, count(*)
from kvk."${newk}"
group by dossiernr, subdossier, vgnummer, hn_1x45, woonplaats
having count(*) > 1


-- if dupes are found. otherwise SKIP to ADD BAG.

-- create duplication tables by finding old records of duplicate so newest records is kept.
drop table kvk.kvk_dupes;
-- we find duplicate records in 2024 and see if duplicate intersect with old records. We need to delete those duplicates.
-- we collect all dupes.
create table kvk.kvk_dupes as (
select ka.* from kvk."${newk}" ka, (
-- check for duplicates --
select dossiernr, subdossier, vgnummer, hn_1x45, woonplaats, count(*)
from kvk."${newk}"
group by dossiernr, subdossier, vgnummer, hn_1x45, woonplaats
having count(*) > 1
) dupes
where ka.dossiernr = dupes.dossiernr and ka.subdossier = dupes.subdossier and ka.vgnummer = dupes.vgnummer and ka.woonplaats = dupes.woonplaats
intersect select * from kvk."${prevk}"
)

select * from kvk.kvk_dupes;
-- delete records present in dupes table from target (actual) table.

DELETE FROM kvk.${newk} AS tgt
USING kvk.kvk_dupes AS src
WHERE tgt.dossiernr=src.dossiernr AND tgt.subdossier=src.subdossier AND tgt.reg_ltr=src.reg_ltr AND tgt.hfdzk_fil=src.hfdzk_fil AND tgt.rsi=src.rsi AND tgt.vgnummer=src.vgnummer AND tgt.rsi_1=src.rsi_1 AND tgt.dsnr_hzkey=src.dsnr_hzkey AND tgt.subd_hzkey=src.subd_hzkey AND tgt.rsi_2=src.rsi_2 AND tgt.hn_1x45=src.hn_1x45 AND tgt.rsi_3=src.rsi_3 AND tgt.hn_1x2x30=src.hn_1x2x30 AND tgt.hn_2x2x30=src.hn_2x2x30 AND tgt.rsi_4=src.rsi_4 AND tgt.hn_1x30=src.hn_1x30 AND tgt.rsi_5=src.rsi_5 AND tgt.straatnaam=src.straatnaam AND tgt.rsi_7=src.rsi_7 AND tgt.huisnr=src.huisnr AND tgt.rsi_8=src.rsi_8 AND tgt.toev_hsnr=src.toev_hsnr AND tgt.rsi_9=src.rsi_9 AND tgt.postcode=src.postcode AND tgt.rsi_11=src.rsi_11 AND tgt.woonplaats=src.woonplaats AND tgt.rsi_12=src.rsi_12 AND tgt.strnaam_ca=src.strnaam_ca AND tgt.rsi_14=src.rsi_14 AND tgt.hsnr_ca=src.hsnr_ca AND tgt.rsi_15=src.rsi_15 AND tgt.toevhn_ca=src.toevhn_ca AND tgt.rsi_16=src.rsi_16 AND tgt.postcd_ca=src.postcd_ca AND tgt.rsi_18=src.rsi_18 AND tgt.filler=src.filler AND tgt.vsi=src.vsi AND tgt.woonpl_ca=src.woonpl_ca AND tgt.rsi_19=src.rsi_19 AND tgt.rv_fijn=src.rv_fijn AND tgt.rsi_20=src.rsi_20 AND tgt.hoofdact=src.hoofdact AND tgt.nevact1=src.nevact1 AND tgt.nevact2=src.nevact2 AND tgt.rsi_21=src.rsi_21 AND tgt.kwp_totaal=src.kwp_totaal AND tgt.rsi_22=src.rsi_22 AND tgt.kwp_ft=src.kwp_ft AND tgt.rsi_23=src.rsi_23 AND tgt."import"=src."import" AND tgt.export=src.export AND tgt.rsi_24=src.rsi_24 AND tgt.telabnr=src.telabnr AND tgt.telnetnr=src.telnetnr AND tgt.rsi_24b=src.rsi_24b AND tgt.ind_faill=src.ind_faill AND tgt.rsi_26=src.rsi_26 AND tgt.ind_surs=src.ind_surs AND tgt.rsi_27=src.rsi_27 AND tgt.beh_knr=src.beh_knr AND tgt.rsi_29=src.rsi_29 AND tgt.fc=src.fc AND tgt.rsi_30=src.rsi_30 AND tgt.nm_1x30=src.nm_1x30 AND tgt.rsi_31=src.rsi_31 AND tgt.nm_1x2x30=src.nm_1x2x30 AND tgt.nm_2x2x30=src.nm_2x2x30 AND tgt.rsi_32=src.rsi_32 AND tgt.nm_1x45=src.nm_1x45 AND tgt.rsi_33=src.rsi_33 AND tgt.briefaanh=src.briefaanh AND tgt.rsi_34=src.rsi_34 AND tgt.aanspreekt=src.aanspreekt AND tgt.rsi_35=src.rsi_35 AND tgt.titel1=src.titel1 AND tgt.titel2=src.titel2 AND tgt.vrletters=src.vrletters AND tgt.vrvoegsels=src.vrvoegsels AND tgt.achternm=src.achternm AND tgt.rsi_36=src.rsi_36 AND tgt.sexe_code=src.sexe_code AND tgt.rsi_37=src.rsi_37 AND tgt.dat_vest=src.dat_vest AND tgt.rsi_38=src.rsi_38 AND tgt.huidig_adr=src.huidig_adr AND tgt.rsi_39=src.rsi_39 AND tgt.dat_inschr=src.dat_inschr AND tgt.rsi_40=src.rsi_40 AND tgt.dat_oprich=src.dat_oprich AND tgt.rsi_41=src.rsi_41 AND tgt.dat_opheff=src.dat_opheff AND tgt.rsi_42=src.rsi_42 AND tgt.dat_voorz=src.dat_voorz AND tgt.rsi_43=src.rsi_43 AND tgt.red_inschr=src.red_inschr AND tgt.rsi_44=src.rsi_44 AND tgt.red_uitsch=src.red_uitsch AND tgt.rsi_45=src.rsi_45 AND tgt.red_opheff=src.red_opheff AND tgt.rsi_46=src.rsi_46 AND tgt.vennaam=src.vennaam AND tgt.rsi_47=src.rsi_47 AND tgt.gestortkap=src.gestortkap AND tgt.rsi_48=src.rsi_48 AND tgt.geplkap=src.geplkap AND tgt.rsi_49=src.rsi_49 AND tgt.maatschkap=src.maatschkap AND tgt.rsi_50=src.rsi_50 AND tgt.stat_insch=src.stat_insch AND tgt.rsi_51=src.rsi_51 AND tgt.exwptotaal=src.exwptotaal AND tgt.rsi_52=src.rsi_52 AND tgt.peildatwp=src.peildatwp AND tgt.rsi_53=src.rsi_53 AND tgt.mobiel_nr=src.mobiel_nr AND tgt.rsi_54=src.rsi_54 AND tgt.url=src.url AND tgt.rsi_55=src.rsi_55 AND tgt.nmi=src.nmi AND tgt.rsi_56=src.rsi_56 AND tgt.rsin=src.rsin AND tgt.rsi_57=src.rsi_57;
--select * from kvk.kvk_actueel_022024;


-- should show a difference.
select count(*) from kvk.${newk} ka;
select count(*) from kvk.kvk_actueel_052024_backup kab ;


-- no dupes should be present.
select dossiernr, subdossier, vgnummer, hn_1x45, woonplaats, count(*)
from kvk.kvk_actueel_032024
group by dossiernr, subdossier, vgnummer, hn_1x45, woonplaats
having count(*) > 1


select * from kvk.kvk_actueel_022024_backup ka where dossiernr = '88981258';
select * from kvk.kvk_actueel_022024 ka where dossiernr = '88981258';
select * from kvk.kvk_actueel_012024 ka where dossiernr = '88981258';
select * from kvk.kvk_actueel_122023 ka where dossiernr = '88981258';
select * from kvk.kvk_actueel_112023 ka where dossiernr = '88981258';


/*
 * ADD BAG.
 *
   Create new BAG nummeraanduiding matched dataset
   Where we match with 1 addresss used for BAG ids / keys and geo locations

 */

drop table if exists kvk.kvk_actueel_bag_062024;
select
    dossiernr,
    subdossier,
    vgnummer,
    hn_1x45,
    hoofdact,
    nevact1,
    nevact2,
    huisnr,
    toev_hsnr,
    nm_1x45,
    dat_vest,
    dat_inschr,
    dat_oprich,
    red_inschr,
    red_uitsch,
    red_opheff,
    vennaam,
    gestortkap,
    stat_insch,
    mobiel_nr,
    rv_fijn,
    url,
    a.*
into kvk.kvk_actueel_bag_062024
from kvk.${newk} ki,
lateral ( select
                --coalesce (pid, sid, lid) as matched_id,
                pa.pid,
                pa.numid,
                pa.vid,
                pa.sid,
                pa.lid,
                pa.huisnummer, pa.huisletter, pa.huisnummertoevoeging,
                pa.postcode,
                pa.oppervlakte,
                levenshtein(concat(pa.huisnummer, ' ', lower(pa.huisletter) , lower(pa.huisnummertoevoeging)),
                            concat(ki.huisnr,     ' ', lower(ki.toev_hsnr) )) as match_score,
                pa.geopunt
          from baghelp.all_pvsl_nums pa
          where pa.postcode = ki.postcode
          -- and pa.huisnummer = r.huisnummerint
          order by levenshtein(concat(pa.huisnummer, ' ', lower(pa.huisletter) , lower(pa.huisnummertoevoeging)),
                               concat(ki.huisnr,     ' ', lower(ki.toev_hsnr) ))  asc
          limit 1
) a


drop table if exists kvk.kvk_bag_missing_062024;
create table kvk.kvk_bag_missing_062024 as (
select * from kvk.kvk_actueel_062024 trg
where not exists (select 1 from kvk.kvk_actueel_bag_052024 src where src.dossiernr=trg.dossiernr)
)


select * from kvk."${newk}" trg
where not exists (select 1 from kvk.kvk_actueel_bag_062024 src where src.dossiernr=trg.dossiernr) and straatnaam != 'Postbus';


--select * from kvk.kvk_bag_missing_042024 where straatnaam != 'Postbus';

-- create index on baghelp.all_pvsl_nums (openbareruimtenaam);

/*
 * Match missing kvk entries to bag addresses
 *
 * */
insert into kvk.kvk_actueel_bag_062024 (
select
    dossiernr,
    subdossier,
    vgnummer,
    hn_1x45,
    hoofdact,
    nevact1,
    nevact2,
    huisnr,
    toev_hsnr,
    nm_1x45,
    dat_vest,
    dat_inschr,
    dat_oprich,
    red_inschr,
    red_uitsch,
    red_opheff,
    vennaam,
    gestortkap,
    stat_insch,
    mobiel_nr,
    rv_fijn,
    url,
    --ki.postcode,
    a.*
from kvk.kvk_bag_missing_062024 ki,
lateral ( select
                --coalesce (pid, sid, lid) as matched_id,
                pa.pid,
                pa.numid,
                pa.vid,
                pa.sid,
                pa.lid,
                pa.huisnummer, pa.huisletter, pa.huisnummertoevoeging,
                pa.postcode,
                pa.oppervlakte,
                levenshtein(concat(pa.openbareruimtenaam, ' ', pa.huisnummer, ' ', lower(pa.huisletter) , lower(pa.huisnummertoevoeging)),
                            concat(ki.straatnaam, ' ', ki.huisnr,     ' ', lower(ki.toev_hsnr) )) as match_score,
                pa.geopunt
          from baghelp.all_pvsl_nums pa
          where
          pa.openbareruimtenaam = trim(regexp_replace(ki.straatnaam, '[^[:alpha:]\s]', '', 'g')) and ki.straatnaam != 'Postbus'
          -- pa.postcode = ki.postcode
          -- and pa.huisnummer = r.huisnummerint
          order by levenshtein(concat(pa.openbareruimtenaam, ' ', pa.huisnummer, ' ', lower(pa.huisletter) , lower(pa.huisnummertoevoeging)),
                               concat(ki.straatnaam, ' ', ki.huisnr,     ' ', lower(ki.toev_hsnr) ))  asc
          limit 1
) a
)

-- check how much we can relate to bag adresses. difference should be minimal and postbus related..
select count(*) from kvk.kvk_actueel_062024;
select count(*) from kvk.kvk_actueel_bag_062024 kav;


/*
 * Add administration codes province - gemeente - wijk - buurt to each record.
 */

drop table if exists kvk.kvk_actueel_bag_codes_062024;
select
    kab.*,
    nbwgp.buurtnaam,
    buurtcode,
    gemeentenaam,
    gemeentecode,
    nbwgp.naam as provincienaam,
    nbwgp.identificatie as provinciecode
into kvk.kvk_actueel_bag_codes_062024
from kvk.kvk_actueel_bag_062024 kab
left outer join baghelp.num_buurt_wijk_gemeente_provincie nbwgp on (nbwgp.numid = kab.numid )

/**

    Create new tables used for publishing.

**/

/**
    Merge kvk vestigingen met CBS SBI codes activiteiten
    uses sbi code from https://gitlab.com/commondatafactory/sbi-codes
    which extracts sbi codes from
    http://sbi.cbs.nl/cbs.typeermodule.typeerservicewebapi
**/

/* main table used to restore database.
 *
 * all activities are expanded.
 * */
drop table if exists kvk.kvk_expand_sbi_new cascade;

select
        ka.*,
        r.omschrijving_rechtsvormcode,
        ST_Transform(ST_SetSRID(geopunt,28992 ), 3857) as geometry,
        code as sbicode,
        title as activiteit,
        sbi_tree -> 'l1' ->> 0 as l1_code,
        sbi_tree -> 'l1' ->> 1 as l1_title,
        sbi_tree -> 'l2' ->> 0 as l2_code,
        sbi_tree -> 'l2' ->> 1 as l2_title,
        sbi_tree -> 'l3' ->> 0 as l3_code,
        sbi_tree -> 'l3' ->> 1 as l3_title,
        sbi_tree -> 'l4' ->> 0 as l4_code,
        sbi_tree -> 'l4' ->> 1 as l4_title,
        sbi_tree -> 'l5' ->> 0 as l5_code,
        sbi_tree -> 'l5' ->> 1 as l5_title
into kvk.kvk_expand_sbi_new
from kvk.kvk_actueel_bag_codes_062024 ka
left outer join kvkimport.rechtsvormcodes r on (r.rechtsvormcode = rv_fijn)
left outer join import.sbicodes s on (
        ka.hoofdact = s.code or
        ka.nevact1 = s.code or
        ka.nevact2 = s.code);


select count(*) from kvk.kvk_expand_sbi_new kesn;
select count(*) from kvk.kvk_expand_sbi kes;

/* replace publishing table */

drop table if exists kvk.kvk_expand_sbi cascade;
alter table kvk.kvk_expand_sbi_new rename to kvk_expand_sbi;

/*
        2023 - 09
        Adjust table to be ready for pg_tileserver

        TODO: script below code with a list of Gemeente Codes.
*/

ALTER TABLE kvk.kvk_expand_sbi ADD COLUMN id SERIAL PRIMARY KEY;
create index on kvk.kvk_expand_sbi (gemeentecode);
CREATE index on kvk.kvk_expand_sbi using gist (geometry);
ALTER table kvk.kvk_expand_sbi alter column geometry type geometry(point, 3857);
comment on table kvk.kvk_expand_sbi is '062024v1';

-- copy anonymous test data into official dataset. --
insert into kvk.kvk_expand_sbi select * from kvk.kvk_expand_sbi_anoniem;

/* Create VIEWS for users */

/*
drop view if exists kvk.kvk_expand_sbi_gm0848;  -- Son en Breugel!
create view kvk.kvk_expand_sbi_GM0848 as ( select * from kvk.kvk_expand_sbi kes where kes.gemeentecode = 'GM0848');
comment on view kvk.kvk_expand_sbi_GM0848  is 'Son en Breugel';

drop view if exists kvk.kvk_expand_sbi_demo_gm0848;  -- DEMO Son en Breugel!
create view kvk.kvk_expand_sbi_demo_GM0848 as ( select * from kvk.kvk_expand_sbi kes where kes.gemeentecode = 'demo_GM0848');
comment on view kvk.kvk_expand_sbi_demo_GM0848  is 'DEMO Son en Breugel';


drop view if exists kvk.kvk_expand_sbi_gm0796; -- 's-hertogenBosch'
create view kvk.kvk_expand_sbi_GM0796 as ( select * from kvk.kvk_expand_sbi kes where kes.gemeentecode = 'GM0796');
comment on view kvk.kvk_expand_sbi_GM0796  is 's-Hertogenbosch';


drop view if exists kvk.kvk_expand_sbi_gm1926; --  Pijnacker-Nootdorp
create view kvk.kvk_expand_sbi_GM1926 as ( select * from kvk.kvk_expand_sbi kes where kes.gemeentecode = 'GM1926');
comment on view kvk.kvk_expand_sbi_gm1926 is 'Pijnackter-Nootdorp';

drop view if exists kvk.kvk_expand_sbi_gm0861;  -- Veldhoven
create view kvk.kvk_expand_sbi_GM0861 as ( select * from kvk.kvk_expand_sbi kes where kes.gemeentecode = 'GM0861');
comment on view kvk.kvk_expand_sbi_gm0861 is 'Veldhoven';

drop view if exists kvk.kvk_expand_sbi_gm0441;  -- schagen
create view kvk.kvk_expand_sbi_GM0441 as ( select * from kvk.kvk_expand_sbi kes where kes.gemeentecode = 'GM0441');
comment on view kvk.kvk_expand_sbi_gm0441 is 'Schagen';

drop view if exists kvk.kvk_expand_sbi_gm0823;  -- oirschot
create view kvk.kvk_expand_sbi_GM0823 as ( select * from kvk.kvk_expand_sbi kes where kes.gemeentecode = 'GM0823');
comment on view kvk.kvk_expand_sbi_gm0823 is 'Oirschot';

drop view if exists kvk.kvk_expand_sbi_gm0637;  -- zoetermeer
create view kvk.kvk_expand_sbi_GM0637 as ( select * from kvk.kvk_expand_sbi kes where kes.gemeentecode = 'GM0637');
comment on view kvk.kvk_expand_sbi_gm0637 is 'Zoetermeer';


select * from kvk.kvk_expand_sbi_gm0796;
select * from kvk.kvk_expand_sbi_gm0848;
select * from kvk.kvk_expand_sbi_demo_gm0848;
select * from kvk.kvk_expand_sbi_gm1926;
select * from kvk.kvk_expand_sbi_gm0861;
select * from kvk.kvk_expand_sbi_gm0441;
select * from kvk.kvk_expand_sbi_gm0823;
select * from kvk.kvk_expand_sbi_gm0637;
*/
select * from kvk.kvk_expand_sbi kes;
select count(*) from kvk.kvk_expand_sbi where gemeentecode = 'DEMO_GM0848'; --'Demo son en breugel'cd
select count(*) from kvk.kvk_expand_sbi where gemeentecode = 'GM0848'; --'Son en breugel'
select count(*) from kvk.kvk_expand_sbi where gemeentecode = 'GM0861'; --'Veldhoven'
select count(*) from kvk.kvk_expand_sbi where gemeentecode = 'GM0796'; --'shertogenbosh'
select count(*) from kvk.kvk_expand_sbi where gemeentecode = 'GM1926'; --'Pijnacker'
select count(*) from kvk.kvk_expand_sbi where gemeentecode = 'GM0823'; --'Oirschot'
select count(*) from kvk.kvk_expand_sbi where gemeentecode = 'GM0861'; --'VeldHoven'
select count(*) from kvk.kvk_expand_sbi where gemeentecode = 'GM0441'; --'Schagen'

select count(*) from kvk.kvk_expand_sbi where gemeentecode = 'GM0362'; --'Amstelveen'
select count(*) from kvk.kvk_expand_sbi where gemeentecode = 'GM0358'; --'Aalsmeer'
select count(*) from kvk.kvk_expand_sbi where gemeentecode = 'GM0361'; --'Alkmaar'

select count(*) from kvk.kvk_expand_sbi where gemeentecode = 'GM0437'; --'OuderAmstel'
select count(*) from kvk.kvk_expand_sbi where gemeentecode = 'GM0451'; --'Uithoorn'
select count(*) from kvk.kvk_expand_sbi where gemeentecode = 'GM0384'; --'Diemen'


select gemeentenaam , gemeentecode, count(*) count from kvk.kvk_expand_sbi group by gemeentenaam, gemeentecode order by count desc;


select count(*) from kvk.kvk_expand_sbi;

--SELECT set_config('log_statement', 'all', false);
